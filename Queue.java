public class Queue{
    private int maxSize;
    private long[] queArray;
    private int front;
    private int rear;
    private int nItems;

    public Queue(int s){ //Constructor
    maxSize = s;
    queArray = new long[maxSize];
    front = 0;
    rear = -1;
    nItems=0;
    }

    public void insert(long j){//เข้าคิว
        if(rear ==maxSize-1){//ถ้าตัวชี้ไปถึงลำดับสุดท้ายให้วนตัวชี้ไปลำดับแรก (Circular Queue)
            rear = -1;
        }
        queArray[++rear]=j;
        nItems++;
    }

    public long remove(){//ออกคิว
        long temp = queArray[front++]; 
        if(front==maxSize){//ถ้าคิวหน้าสุดโดนเอาออก(ขยับเก้าอี้) ให้วน index หน้าสุดไปตำแหน่งที่ 0 ใน Array อีกครั้ง
            front = 0;
        }
        nItems--;
        return temp;
    }

    public long peekFront(){
        return queArray[front];
    }

    public boolean isEmpty(){
        return (nItems==0);
    }

    public boolean isFull(){
        return (nItems==maxSize);
    }

    public int size(){
        return nItems;
    }
}